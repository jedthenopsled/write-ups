#!/usr/bin/python3

import sys
import string
import argparse
import itertools


def warn(*objs):

    print("WARNING: ", *objs, file=sys.stderr)
    sys.exit(1)


def get_string(length):

    if length <= 0:
        warn('invalid length')
        sys.exit(1)

    to_gen = int(length / 3)
    if length % 3:
        to_gen += 1

    value_generator = itertools.product(list(string.ascii_uppercase), list(
        string.ascii_lowercase), list(string.digits))

    eip_string = ''
    for i, val in enumerate(value_generator):
        if i == to_gen:
            break
        eip_string += ''.join(val)

    return eip_string


def get_offset(length, eip_addr):

    eip_string = get_string(length)
    eip_addr = eip_addr.upper()
    eip_addr = eip_addr.replace('0X', '') if eip_addr.startswith('0X') else \
        eip_addr
    eip_addr = "".join(reversed([eip_addr[i:i+2] for i in range(0, len(
        eip_addr), 2)]))

    try:
        eip_addr = bytes.fromhex(eip_addr).decode('ascii')
    except:
        warn('invalid eip_addr')

    if len(eip_addr) != 4:
        warn("invalid eip_addr")
    try:
        print(eip_string.index(eip_addr))
    except:
        warn('eip_addr not found')


def main():

    parser = argparse.ArgumentParser(prog='eip_offset')
    parser.add_argument('-l', '--length', type=int, required=True,
                        help='length of the string')
    parser.add_argument('-a', '--address', type=str, nargs='?', help='address \
        of eip after overflowing')
    args = parser.parse_args()

    if args.address and args.length:
        get_offset(args.length, args.address)
    elif args.length:
        print(get_string(args.length))
    else:
        parser.print_help()
        sys.exit(1)


if __name__ == '__main__':

    main()
