# Collision

Here is what we know after reading the source code:

 - The program expects a parameter which is 20 bytes long
 - The parameter is casted to int*
 - The sum of the 20 bytes parameter must be equal to `hashcode = 0x21DD09EC`

These conditions must be respected in order to execute `system("/bin/cat flag");`

----------

# Solution

Since an int is 4bytes long and a char is 1byte long, the flag is revealed when the sum of a 20 bytes int is equal the `hashcode`.
Because of strlen, we can't just pass `python -c 'print "\xec\x09\xdd\x21" + "\x00" * 16'`  so I choose to use junk data instead of \x00 and to substract the value of the junk data to the hashcode

```col@ubuntu:~$ python -c "print '0x{:x}'.format(0x21dd09ec - 0x02020202 * 4)"
0x19d501e4```

now when using this value in our payload we have access to the flag

   ./col `python -c 'print "\xe4\x01\xd5\x19" + "\x02" * 16'`