# fd

 After quickly looking the code of fd.c, we notice that the program takes an argument in parameter.
 The program converts argv[1] to int, substract 0x1234 (4660 in decimal) and uses the result as a file descriptor.
 The aim is to be able to input "LETMEWIN\n" so that `system("/bin/cat flag");` can be executed.


----------


 
## Solution

 A way of solving this is to pass 4660 as a parameter, to get a file descriptor equal to 0 (stdin). From there you can manually input "LETMEWIN\n" 