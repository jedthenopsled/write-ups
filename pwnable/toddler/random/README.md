# random

After a quick look at the code, here's what we know:

	- The flag is displayed if the result of key XOR random equals 0xdeadbeef
		where key is filled via user input and random is randomly generated number

# solution

A quick solution would be to input the integer value of 0xdeadbeef
```
@jed ➜  passcode git:(master) echo $((16#deadbeef))
3735928559
```
in this case if random equals 0 the program enters the if statement and it's a win.
Wouldn't it be great if we could control the return of rand() ?
Actually we could ;)
I can create my own shared library which contains a custom rand() function returning only 0
and inject it via the envrinment variable $LD_PRELOAD.
But this challenge can be solved even more easily, the rand function is not seeded.
By default
`If no seed value is provided, the functions are automatically seeded with a value of 1.`

Which means the first call to rand() will always return the same value. To find out which value
I write a simple C program

```
#include <stdlib.h>
#include <stdio.h>

int	main(void)
{
	printf("%d\n", rand());
	return (0);
}

random@ubuntu:/tmp/foobar$ ./a.out
1804289383
```

and since 

```
A ^ B = C
C ^ B = A
```

then
```
key ^ random = 0xdeadbeef
random ^ 0xdeadbeef = key
1804289383 ^ 3735928559 = 3039230856
```
when using this key, the program outputs the flag
```
random@ubuntu:~$ ./random
3039230856
Good!
... flag here ...
```
