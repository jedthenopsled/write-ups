# flag

We only have access to a binary, flag.
First I try to get some information on this binary

    @jed ➜  flag git:(master) ✗ file flag
	flag: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), statically linked, stripped

when running strings I get some interesting informations

    @jed ➜  flag git:(master) ✗ strings flag
	...
	$Info: This file is packed with the UPX executable packer http://upx.sf.net $
	...

----------


# solution

After compiling upx, I unpack the binary

    ./upx -d ~/Projects/write-ups/pwnable/toddler/flag/flag

and after running strings again on the binary, I find something similarly formatted to the previous flags of pwnable.kr, it works !